#### Beschreibung ####
Dieses Projekt zeigt wie man mit einem Arduino ESP8266 NodeMCU LUA WiFi Entwickler Board eine wifi Verbindung aufbauen kann und über diese Informationen auf einem I²C LCD Display anzeigen kann.

#### Projekt Dokumentation ####
Befindet sich unter /doku.

#### Quellen ####
https://defendtheplanet.net/2016/10/17/building-a-wifi-liquidcrystal-display-esp8266-for-less-than-10-eur/

https://github.com/PaulPetring/esp8266-liquid-display/blob/master/main.ino

http://www.sunfounder.com/wiki/index.php?title=I%C2%B2C_LCD1602
