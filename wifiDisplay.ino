#include <LiquidCrystal_I2C.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

const char* ssid = "YOUR-SSID";
const char* password = "PASSWD"; 

String Line0 = "";
String Line1 = "";

// Construct an LCD object and pass it the 
// I2C address, width (in characters) and
// height (in characters). Depending on the
// Actual device, the IC2 address may change.
LiquidCrystal_I2C lcd(0x27, 16, 2);

ESP8266WebServer server(80);

void showText(String supposed, int line)
{
  if (line==0) {
    Line0 = supposed; }
  else { Line1 = supposed; }
  
  lcd.setCursor(0, line);
  lcd.print("                ");
  lcd.setCursor(0, line);
  lcd.print(supposed);
}

void reset() {
  showText("",0);
  showText("",1);

  server.send(200, "text/plain", "Display gelöscht");
}

void set() {
  String data = server.arg("data");
  String line = server.arg("line");
  if(line=="1" ) {
    showText(data,1);  
  } else {
    showText(data,0);  
  }
  
  server.send(200, "text/plain", data);
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void handleRoot() {
  server.send(200, "text/plain", Line0 + "\n" + Line1);
}

void setup() {
  // The begin call takes the width and height. This
  // Should match the number provided to the constructor.
  lcd.begin(16,2);
  lcd.init();

  // Turn on the backlight.
  lcd.backlight();

  // Move the cursor characters to the right and
  // zero characters down (line 1).
  //lcd.setCursor(1, 0);

  // Print HELLO to the screen, starting at 5,0.
  //lcd.print("My Get Shit");

  // Move the cursor to the next line and print
  // WORLD.
  //lcd.setCursor(4, 1);      
  //lcd.print("done Arduino");
  
  pinMode(LED_BUILTIN, OUTPUT);
  
  lcd.display();
  lcd.begin(16, 2);

  showText("Connecting wifi",0);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    showText("waiting..",1);
  }
  showText("done.",1);

  
  showText("Starting Server",0);
  server.begin();

  server.on("/set", set); 
  server.on("/reset", reset); 
  server.on("/", handleRoot); 

  server.onNotFound(handleNotFound);
  server.begin();
  
  showText(WiFi.localIP().toString(),0);  
}

void loop() {
  server.handleClient();
}
